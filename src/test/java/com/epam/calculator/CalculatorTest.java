package com.epam.calculator;

import org.junit.Assert;
import org.junit.Test;

public class CalculatorTest {

    @Test
    public void add(){
        double result = Calculator.add(5,5);
        Assert.assertEquals(10,result);
        result = Calculator.add(2.5,3.5);
        Assert.assertEquals(6, result);
        result = Calculator.add(7.3, 0);
        Assert.assertEquals(7.3, result);
        result = Calculator.add(-5, 5);
        Assert.assertEquals(0, result);
    }

    @Test
    public void sub(){
        double result = Calculator.sub(10,5);
        Assert.assertEquals(5,result);
        result = Calculator.sub(-10,5);
        Assert.assertEquals(-15, result);
        result = Calculator.sub(-10,-5);
        Assert.assertEquals(-5, result);
        result = Calculator.sub(0,-5);
        Assert.assertEquals(5, result);
    }

    @Test
    public void mult(){
        double result = Calculator.mult(2,5);
        Assert.assertEquals(10,result);
        result = Calculator.mult(-10,5);
        Assert.assertEquals(-50,result);
        result = Calculator.mult(0,5);
        Assert.assertEquals(0,result);
        result = Calculator.mult(-10,-5);
        Assert.assertEquals(50,result);
    }

    @Test
    public void div(){
        double result = Calculator.div(10,5);
        Assert.assertEquals(2,result);
        result = Calculator.div(0,5);
        Assert.assertEquals(0,result);
        result = Calculator.div(-5,5);
        Assert.assertEquals(-1,result);
    }
    @Test(expected = IllegalArgumentException.class)
    public void div_argument_exception(){
        double result = Calculator.div(10,0);
    }

    @Test
    public void sqrt(){
        double result = Calculator.sqrt(4);
        Assert.assertEquals(2,result);
        result = Calculator.sqrt(0);
        Assert.assertEquals(0,result);
    }
    @Test(expected = IllegalArgumentException.class)
    public void sqrt_argument_exception(){
        double result = Calculator.sqrt(-5);
    }
}
