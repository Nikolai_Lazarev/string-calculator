package com.epam.calculator;

import org.junit.Assert;
import org.junit.Test;

public class PropertiesManagerTest {

    public static final String CORRECT_INPUT_PATTERN_FOR_CALCULATOR = "^((\\-?(sqrt)?\\d+(\\.\\d+)?) [+\\-*\\/] )*(\\-?(sqrt)?\\d+(\\.\\d+)?)$";
    PropertiesManager propertiesManager = new PropertiesManager();

    @Test
    public void get_input_pattern(){
        String result = propertiesManager.getInputPattern();
        Assert.assertEquals(CORRECT_INPUT_PATTERN_FOR_CALCULATOR, result);
    }

}
