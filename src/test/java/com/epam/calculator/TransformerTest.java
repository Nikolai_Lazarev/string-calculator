package com.epam.calculator;

import org.junit.Assert;
import org.junit.Test;

public class TransformerTest {

    Transformer transformer =  new Transformer();

    @Test
    public void transform_string_to_string_array(){
        String [] result = transformer.transformToStringArray("5 + 3 * 4");
        Assert.assertEquals(new String[]{"5","+","3","*","4"}, result);
    }

}
