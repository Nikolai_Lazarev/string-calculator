package com.epam.calculator;

import org.junit.Assert;
import org.junit.Test;

public class UserManagerTest {

    @Test
    public void print_fake_input_warning(){
        String result = UserManager.fakeInputWarning();
        Assert.assertEquals("Введите выражение в соответствии с правилами."+UserManager.helpMessage()+UserManager.exitMessage(), result);
    }

    @Test
    public void print_operation_for_exit(){
        String result = UserManager.exitMessage();
        Assert.assertEquals(" Для выхода введите \"exit\".", result);
    }

    @Test
    public void print_operation_for_help(){
        String result = UserManager.helpMessage();
        Assert.assertEquals(" Для получения справки введите \"help\".", result);
    }

    @Test
    public void input_expression_request(){
        String result = UserManager.inputRequest();
        Assert.assertEquals("Введите выражение", result);
    }

    @Test
    public void print_requirements(){
        String result = UserManager.getRequirements();
        Assert.assertEquals("\n--------------------------\n"+
                "Отделяйте числа и операции пробелами: 5 + 3 * 4\n" +
                "Доступны следующие операции\n" +
                "Сложение: a + b\n" +
                "Вычитание: a - b\n" +
                "Умножение: a * b\n" +
                "Деление: a / b\n" +
                "Квадратный корень: sqrta, sqrtb\n" +
                "--------------------------\n", result);
    }

    @Test
    public void check_user_input(){
        boolean result = UserManager.checkUserInput("2 + 3 * 5 + 5");
	    Assert.assertTrue(result);
        result = UserManager.checkUserInput("help");
	    Assert.assertFalse(result);
        result = UserManager.checkUserInput("ae 4 212 1");
	    Assert.assertFalse(result);
        result = UserManager.checkUserInput("5+3+1");
	    Assert.assertFalse(result);
        result = UserManager.checkUserInput("5 + 3 + -1");
	    Assert.assertTrue(result);
        result = UserManager.checkUserInput("sqrt-5 + 3 + -1");
	    Assert.assertFalse(result);
    }
}
