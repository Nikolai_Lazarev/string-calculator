package com.epam.calculator;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;


public class Runner {

	private static final Logger logger = Logger.getLogger(Runner.class);

	public static void main(String[] args) {
		Transformer transformer = new Transformer();
		Count count = new Count();
		String arr[];
		logger.info(UserManager.getRequirements());
		logger.info(UserManager.helpMessage());
		logger.info(UserManager.exitMessage());
		logger.info(UserManager.inputRequest());
		while (true) {
			String input = UserManager.userInput();
			if (UserManager.checkUserInput(input)) {
				arr = transformer.transformToStringArray(input);
				ArrayList<String> myArr = new ArrayList<String>(Arrays.asList(arr));
				logger.info(count.counting(myArr));
			}
		}
	}
}
