package com.epam.calculator;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserManager {

    public static String userInput(){
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    public static String fakeInputWarning(){
        return "Введите выражение в соответствии с правилами."+helpMessage()+exitMessage();
    }

    public static String inputRequest(){
        return "Введите выражение";
    }

    public static String exitMessage(){
        return " Для выхода введите \"exit\".";
    }

    public static String helpMessage(){
        return " Для получения справки введите \"help\".";
    }

    public static String getRequirements(){
        return "\n--------------------------\n"+
                "Отделяйте числа и операции пробелами: 5 + 3 * 4\n" +
                "Доступны следующие операции\n" +
                "Сложение: a + b\n" +
                "Вычитание: a - b\n" +
                "Умножение: a * b\n" +
                "Деление: a / b\n" +
                "Квадратный корень: sqrta, sqrtb\n" +
                "--------------------------\n";
    }

    public static boolean checkUserInput(String str){
        if(str.trim().equals("exit")){
            System.exit(0);
        }
        if(str.trim().toLowerCase().equals("help")){
            System.out.println(getRequirements());
            System.out.println(helpMessage());
            System.out.println(exitMessage());
            return false;
        }
        Pattern pattern = Pattern.compile(new PropertiesManager().getInputPattern());
        Matcher matcher = pattern.matcher(str);
        if(matcher.matches())return true;
        System.out.println(fakeInputWarning());
        return false;
    }

}
