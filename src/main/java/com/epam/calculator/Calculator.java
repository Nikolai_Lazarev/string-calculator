package com.epam.calculator;

import com.epam.webcalculator.CalculatorSoap;

class Calculator {
	private static com.epam.webcalculator.Calculator calculator = new com.epam.webcalculator.Calculator();
	private static CalculatorSoap calculatorSoap = calculator.getCalculatorSoap();

	public static double add(double a, double b) {
		return calculatorSoap.add((int) a, (int) b);
	}

	public static double sub(double a, double b) {
		return calculatorSoap.subtract((int) a, (int) b);
	}

	public static double mult(double a, double b) {
		return calculatorSoap.multiply((int) a, (int) b);
	}

	public static double div(double a, double b) {
		if (b == 0) {
			throw new IllegalArgumentException();
		} else {
			return calculatorSoap.divide((int) a, (int) b);
		}

	}

	@Deprecated
	public static double sqrt(double a) {
		if (a < 0) {
			throw new IllegalArgumentException();
		} else return Math.sqrt(a);
	}
}
