package com.epam.calculator;

import java.io.FileInputStream;
import java.util.Properties;
import org.apache.log4j.Logger;

public class PropertiesManager {

    private static final String linkToFile = "src/main/resources/patterns.properties";
    private static final Logger logger = Logger.getLogger(PropertiesManager.class);
    private Properties properties;

    public PropertiesManager() {
        try {
            properties = new Properties();
            properties.load(new FileInputStream(linkToFile));
        } catch (Exception e){
            logger.error(e);
        }
    }

    public String getInputPattern(){
        return properties.getProperty("inputPattern");
    }
}
