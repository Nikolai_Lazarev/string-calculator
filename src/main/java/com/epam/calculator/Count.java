package com.epam.calculator;

import java.util.ArrayList;

public class Count {
	public int searchIndex(ArrayList<String> arr) {
		for (int i = 0; i < arr.size(); i++) {
			if (arr.get(i).equals("*") || arr.get(i).equals("/")) {
				return i - 1;
			}
		}
		return 0;
	}

	@Deprecated
	public double checkSqrt(String element) {
		if (element.length() < 5) return Double.valueOf(element);
		return Calculator.sqrt(Double.valueOf(element.substring(4)));
	}

	public String counting(ArrayList<String> arr) {
		if (arr.size() > 1) {
			while (arr.size() > 1) {
				int startIndex = searchIndex(arr);
				double a = Double.valueOf(arr.get(startIndex));
				double b = Double.valueOf(arr.get(startIndex + 2));
				double result;
				String operation = arr.get(startIndex + 1);
				switch (operation) {
					case "*":
						result = Calculator.mult(a, b);
						break;
					case "+":
						result = Calculator.add(a, b);
						break;
					case "-":
						result = Calculator.sub(a, b);
						break;
					case "/":
						result = Calculator.div(a, b);
						break;
					default:
						result = 0;
						break;
				}

				arr.set(startIndex, String.valueOf(result));
				arr.remove(startIndex + 1);
				arr.remove(startIndex + 1);
				counting(arr);
			}
		}
		return arr.get(0);
	}
}
